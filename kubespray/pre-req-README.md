# Kubespray Airgap Install Pre-Requisites

This document will detail the steps required to provision the simulated air gap environment using terraform to build the infrastructure. After completing these steps, you will be able to follow the existing documentation to build your Secure Container Platform with Kubernetes in an air gapped environment.

## Step 1: Get Permissions

Get the correct access for the following from George at AL:

- automationlogic secure-container-platform project on Github (https://github.com/automationlogic/secure-container-platform)
- AWS secureplatform role

## Step 2: Provisioning the simulated air gap environment

You should complete these steps on your **local machine**. The purpose of this section is to gain access to the repository containing the essential terraform scripts to build the infrastructure for our kubespray cluster.

```bash
Git clone https://github.com/automationlogic/secure-container-platform.git
cd secure-container-platform
```

The old documentation then tells you to terraform init, but we discovered the following pre-requisite that had not been documented:

## Step 3: Connecting to AWS


Solution: our machine is not communicating with the correct aws account therefore we need to use AWS CLI to authenticate. This error occurs because the terraform state is saved on S3. 

The purpose of this section is to configure AWS CLI with the securecontainerplatform, so we do not get the following error:
Error: error configuring S3 Backend: no valid credential sources for S3 Backend found.

```bash
brew install AWS CLI
export AWS_PROFILE=scp
mkdir .aws
cd .aws
```

Then, you will create this file: `.aws/config` containing the contents below:

```nano
[profile scp]
region = eu-west-2
output = yaml
source_profile = default
role_arn = arn:aws:iam::513848379776:role/OrganizationAccountAccessRole
```

Now, you can confirm you are connected to AWS successfully using `aws s3 ls`

Then enter the following to create a symlink, then use terraform to build the infrastructure

```bash
ln -s ~/Downloads/credentials ~/.aws/credentials
cd secure-container-platform/sage/terraform/common
terraform init
terraform apply
```

By using the symlink it means you don't have to copy the contents of the credentials over each time. The reason it wasn't working before is because we didn't set a profile in the terraform so it was trying to use the default credentials, but we were only updating the scp credentials.

Once the terraform apply command has finish running, you should recieve the following output:

```bash
Apply complete! Resources: 23 added, 0 changed, 3 destroyed.

Outputs:

bastion_public_ip = "18.133.141.152"
haproxy_private_ip = ""
nexus_private_ip = ""
ssh-to-bastion = "ssh default"
```

Now that the infrastructure has been built with terraform, log into the AWS console and follow the Kubespray Airgap Install steps (https://automationlogic.atlassian.net/wiki/spaces/JT/pages/3334275073/Kubespray+Airgap+Install+steps)
