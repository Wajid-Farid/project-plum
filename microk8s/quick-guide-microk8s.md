# Quick Setup Version of Offline Microk8s

A condensed version of the "offline-microk8s.md" document, designed for a quick setup of offline Microk8s via a minimal set of scripts. For an in-depth contextualised setup guide, please see "offline-microk8s.md" on Bitbucket: https://bitbucket.org/Wajid-Farid/project-plum/src/master/microk8s/.


## Prerequisites
- AWS simulated offline environment (see "offline-microk8s.md" for details).
- Ensure VMs are ubuntu 20.04 servers.
- Security key saved & added to authentication agent on local machine.
- SSH'd into all VMs (1 Bastion & 3 private)


### [1] Git Clone Bitbucket Repository

First, in the Bastion we must Git clone the project repo, to make available required files & scripts.

```bash
$ git clone https://Wajid-Farid@bitbucket.org/Wajid-Farid/project-plum.git
```
### [2] Prepare Required Scripts to be Executable

We need to enter the directory containing the scripts via the path: "project-plum/microk8s/microk8s-scripts". Then, we need to Chmod the scripts, so:

```bash
$ chmod +x ~/project-plum/microk8s/microk8s-scripts/scp-k8.sh
$ chmod +x ~/project-plum/microk8s/microk8s-scripts/install-microk8s.sh
$ chmod +x ~/project-plum/microk8s/microk8s-scripts/install-docker.sh
$ chmod +x ~/project-plum/microk8s/microk8s-scripts/install-docker-microk8s.sh
```

**NOTE**: Stay in ~/ throughout the process.

### [3] Install Core/Microk8s Snaps on Bastion & Copy/Install on All Nodes

To Download the Core/Microk8s snaps & to send the Installation scripts to the offline nodes, we need to:

```bash
$ . ~/project-plum/microk8s/microk8s-scripts/scp-k8.sh #also SCPs deployment file across (for use later)
```

Then, we are required to SSH into ALL nodes (3 offline nodes) and install microk8s by:

```bash
$ ./install-microk8s.sh #this script also adds ubuntu to Microk8s group
```

Microk8s doesn't run upon installation & therefore requires stopping/starting by:

```bash
$ microk8s.status #will show "microk8s is not running..." output
$ microk8s.start
$ microk8s.stop
```

DNS also needs to be enabled for deployment:
```bash
$ microk8s enable dns
```

### [4] Create K8 Cluster by Joining Nodes

At this point, we have Microk8s installed on all offline VMs, however, we haven't assigned master/worker nodes or joined the cluster.

To assign the 'master' role to one of the VMs. The node that runs the 'add-node' command automatically becomes the master node. So choose one VM and do:
```bash
$ microk8s add-node #do it TWICE, to generate join outputs for both remaining nodes
```

This will give output in the form:
```bash
>> Join node with: microk8s join 172.31.111.187:25000/EFuOnQEkHupoEhiENjckElTMfMOyQALw
```

Now, join on each of 2 remaining nodes by:
```bash
$ microk8s join 172.31.111.187:25000/EFuOnQEkHupoEhiENjckElTMfMOyQALw
```

Then check that the nodes are 'Ready':
```bash
$ microk8s.kubectl get nodes
```

It's also useful at this stage to create a namespace for later use:
```bash
$ microk8s.kubectl create ns development
```

Now you have a functioning k8 cluster, the next step is to pull a Docker image before deploying!


### [5] Install Docker on Bastion & Send Required Files to Master Node (to Install)

Now, we need to return to the bastion & Install Docker & pull an nginx image. First, we need to Install Docker + pull the image on the Bastion & send across the Installation & image files to the Master node (NOT REQUIRED ON WORKER NODES) by:
```bash
$ . ~/project-plum/microk8s/microk8s-scripts/install-docker.sh
```

Here, Docker should be installed with a docker image saved as a file. If you SSH into the Master, you'll notice a "install-docker-microk8s.sh" script. This has the Docker installation & image files. Run this by:
```bash
$ ./install-docker-microk8s.sh
```

We then followed Microk8s recommendation and created the folling file in "/etc/docker/daemon.json":
```bash
{
    "insecure-registries" : ["localhost:32000"] 
}
```

In addition, they recommended altering the IP tables policy to enable traffic forwarding:
```bash
$ sudo iptables -P FORWARD ACCEPT
```

This will also save the nginx image as a .tar file and import it. This was to produce a custom local label of the docker image to communicate with the deployment file.


### [6] Attempt Deployment on Master Node of nginx image

Next, we need to attempt deployment. This was an initial problem for our engineering team as we were unable to successfuly deploy the nginx app. However, to attempt deployment, we:

- Created a deployment.yml file (which includes the service.yml).
- Applied the deployment file through kubectl.

The deployment file is already cofngiured and available and was earlier sent through the "scp-k8.sh" script. So in the master, we are required to apply the deployment & services by:
```bash
$ microk8s.kubectl apply -f deployment.yml -n development #"development" is the namespace we created earlier
```

However, the pods do not become 'Ready' & hence the deployment of the nginx app is unsuccessful.

### [7] Potential Solution

Note though, that this exact process worked on the RPI offline cluster, hence it would be important to evaluate or at least compare the cluster set-up, to see if any prior simulation vs physical build configurations made a difference. Due to the resources & deadline (plus considering the priority of the RPI), we were unable to explore further options to help deplpoy the app. However, using an independant VM as a 'private registry' might provide a viable solution for the problem. This can be considered for future developments.



