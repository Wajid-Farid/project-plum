# Set Param  eters to Pre-define Secure-Copy/File Destinations
MASTER_IP=$1
WORKER1_IP=$2
WORKER2_IP=$3

# Download Core/Microk8s Snaps on Bastion
snap download microk8s --channel=1.17/stable
snap download core

# Secure-Copy Core Files for Installation (to Offline Nodes)
scp core_11993.assert ubuntu@$MASTER_IP:~/
scp core_11993.assert ubuntu@$WORKER1_IP:~/
scp core_11993.assert ubuntu@$WORKER2_IP:~/

scp core_11993.snap ubuntu@$MASTER_IP:~/
scp core_11993.snap ubuntu@$WORKER1_IP:~/
scp core_11993.snap ubuntu@$WORKER2_IP:~/

# Secure-Copy Microk8s Installation Snap Files (to Offline Nodes)
scp microk8s_1916.assert ubuntu@$MASTER_IP:~/
scp microk8s_1916.assert ubuntu@$WORKER1_IP:~/
scp microk8s_1916.assert ubuntu@$WORKER2_IP:~/

scp microk8s_1916.snap ubuntu@$MASTER_IP:~/
scp microk8s_1916.snap ubuntu@$WORKER1_IP:~/
scp microk8s_1916.snap ubuntu@$WORKER2_IP:~/

# Secure-Copy Script for Installation Commands & User-Group Modification (to Offline Nodes)
scp ~/project-plum/microk8s/microk8s-scripts/install-microk8s.sh ubuntu@$MASTER_IP:~/
scp ~/project-plum/microk8s/microk8s-scripts/install-microk8s.sh ubuntu@$WORKER1_IP:~/
scp ~/project-plum/microk8s/microk8s-scripts/install-microk8s.sh ubuntu@$WORKER2_IP:~/

# Secure-Copy Script for Docker Installation (to Offline Master Nodes) 
scp ~/project-plum/microk8s/microk8s-scripts/install-docker-microk8s.sh ubuntu@$MASTER_IP:~/

# Secure-Copy Deployment File for K8 (to Offline Master Nodes)
scp ~/project-plum/microk8s/microk8s-scripts/deployment.yml ubuntu@$MASTER_IP:~/