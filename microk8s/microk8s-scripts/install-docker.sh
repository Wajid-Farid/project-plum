# Define Parameters
MASTER_IP=$1

# Create Docker Folder & .Deb Files
mkdir docker_debs
touch ./docker_debs/docker_1.deb ./docker_debs/docker_2.deb ./docker_debs/docker_3.deb ./docker_debs/docker_4.deb

# Install Docker on Bastion & Save Files in Docker_Deb Folder
curl https://download.docker.com/linux/ubuntu/dists/focal/pool/stable/amd64/containerd.io_1.4.9-1_amd64.deb > ./docker_debs/docker_1.deb

curl https://download.docker.com/linux/ubuntu/dists/focal/pool/stable/amd64/docker-ce-cli_20.10.9~3-0~ubuntu-focal_amd64.deb > ./docker_debs/docker_2.deb

curl https://download.docker.com/linux/ubuntu/dists/focal/pool/stable/amd64/docker-ce-rootless-extras_20.10.9~3-0~ubuntu-focal_amd64.deb > ./docker_debs/docker_3.deb

curl https://download.docker.com/linux/ubuntu/dists/focal/pool/stable/amd64/docker-ce_20.10.9~3-0~ubuntu-focal_amd64.deb > ./docker_debs/docker_4.deb

# Secure-Copy Across the Docker Folder to Master Node
scp -r docker_debs ubuntu@$MASTER_IP:~/

# Install Docker on Bastion
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io

# Add 'Docker' to Ubuntu Group 
sudo usermod -aG docker ubuntu 

# Secure-Copy Docker Installation Commands Script to Master Node
scp ~/project-plum/microk8s/microk8s-scripts/install-docker-microk8s.sh ubuntu@$MASTER_IP:~/

# Pull & Transfer Image to Offline Master Node
docker pull nginx
docker save -o nginx_image.docker nginx
scp nginx_image.docker ubuntu@$MASTER_IP:~/

# Exit to Make Changes to Groups
exit