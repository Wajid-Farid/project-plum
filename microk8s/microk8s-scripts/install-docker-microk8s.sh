# Install Docker in Offline Master Node
sudo dpkg -i docker_debs/docker_1.deb docker_debs/docker_2.deb docker_debs/docker_3.deb docker_debs/docker_4.deb

# Load nginx Image
sudo docker load -i nginx_image.docker

# Import Docker Image
sudo docker save nginx > nginx.tar
sudo microk8s ctr image import nginx.tar
sudo microk8s ctr images ls

# Add Docker to 'Ubuntu' Group for Permissions
sudo usermod -aG docker ubuntu

# Exit to Affect User Group Changes
exit