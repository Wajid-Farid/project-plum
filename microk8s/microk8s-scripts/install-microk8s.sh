# Core Installation Commands for Microk8s (To be Implemented on Master, N1 & N2)
sudo snap ack core_11993.assert
sudo snap install core_11993.snap

# Installation Commands for Microk8s (To be Implemented on Master, N1 & N2)
sudo snap ack microk8s_1916.assert
sudo snap install microk8s_1916.snap --classic

# Add Microk8s to Ubuntu User Group
sudo usermod -a -G microk8s ubuntu
sudo chown -f -R ubuntu ~/.kube
newgrp microk8s