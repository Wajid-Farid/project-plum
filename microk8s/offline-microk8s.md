# Microk8s Offline Simulation

This document is an overview of the prerequisites, steps and resources required to create a simulated offline kubernetes cluster, using AWS and Microk8s. Stucturally, the document will list the AWS VM and networking prerequisites, before providing a step-by-step guide in setting-up microk8s offline. Major Resources used will be attached for reference. Additionally, a section will be added on troubleshooting and breakdown of issues experienced by the engineering team. Its important to note that this document is a technical breakdown - hence including (or at least referencing) the code implemented.


**NOTE:** This is an in-depth guide, which is significantly manual. The purpose of this is to provide context to the commands used. For a quick-setup guide, see the "quick-guide-microk8s.md" document (https://bitbucket.org/Wajid-Farid/project-plum/src/master/microk8s/quick-guide-microk8s.md)

## Section 1 - AWS Prerequisites

For our simulated offline set-up, a 3-VM framework contributed as the offline kubernetes cluster, in addition to a bastion acting as the sole source of internet-access. However, all content from the bastion to the cluster was transported through the secure-copy command, which played the role of a media device, as is prevalent in communication for physically offline machines. The security keys utilised will be provided seperately in a secure handover document. Note, an AWS account is required to complete the section ahead (https://automationlogic.atlassian.net/wiki/spaces/AWS/pages/22052876/AWS). 


### **AWS Prerequisites**

#### ***1.1 VPCs, Subnets & Routing:***

##### Private Subnet
- Create Private Subnet (no public IP assigned to VMs create inside) & associate with private route table.


#### ***1.2 AMIs, VMs, Subnets & Security Groups:*** 

##### Bastion Security Group (bastion_sg)
- Create SG with Inbound SSH rule from the IP address of the user. This will allow for access to the bastion via SSH from the user-only. 
- Rule 1: SSH, IPV4_address of User.


##### Microk8s Cluster Security Group (microk8s_cluster_sg)
- Create SG with Inbound rules allowing for SSH into private cluster VMs via bastion, in addition to communication between VMs.
- Rule 1: SSH, IPV4_address of User.
- Rule 2: SSH, IPV4 CIDR of Public Subnet (where bastion resides).
- Rule 3: All Traffic, IPV4 CIDR of Private Subnet (where offline VMs reside).


##### Bastion Server
- VM in public subnet with ability to access internet (NOTE: the only machine in the framework thats able to access internet).
- AMI - Ubuntu Server 20.04 LTS (HVM).
- Instance Type - t2.micro.
- VPC - Default.
- Subnet - Default.
- Security Group - bastion_sg
- Storage Size (GiB) - 8.


##### Master Node
- Master Node/control plane in the private subnet, with no internet access, & the following specification: 
- AMI - Ubuntu Server 20.04 LTS (HVM).
- Instance Type - t3a.large.
- VPC - Default.
- Subnet - microk8s_private.
- Security Group - microk8s_cluster_sg.
- Storage Size (GiB) - 30.


##### Worker Node X2
- Master Node/control plane in the private subnet, with no internet access.
- AMI - Ubuntu Server 20.04 LTS (HVM).
- Instance Type - t2.medium.
- VPC - Default.
- Subnet - microk8s_private.
- Security Group - microk8s_cluster_sg.
- Storage Size (GiB) - 20.


## Section 2 - Microk8s Offline Installation
Prior to installing Microk8s offline, an installation snap is required to be downloaded on the bastion. This is then secure-copied to the master & worker nodes (node assigning will be specified after installation). Generally the core snap is not required in addition to the microk8s snap (if we install the latest version of microk8s). However, using the latest version caused issues with starting high-availability kubernetes, hence an older version of microk8s was installed (requiring core snap usage).


#### ***2.1 Add key to SSH Authentication Agent & Login to Machines***

In the local machine:

```bash
$ ssh-add -k <key_name> 
$ ssh -A ubuntu@<bastion_ip_public>
```


Do the above in 4 independant terminals, then SSH into the 3 offline VMs:

```bash
$ ssh -A ubuntu@<master_ip_private>
$ ssh -A ubuntu@<worker_node1_ip_private>
$ ssh -A ubuntu@<worker_node2_ip_private>
```

**NOTE**: You should now be SSH'd into a bastion and 3 offline machines.<br />

#### ***2.2 Download & Install Microk8s Offline***

a. In the Bastion, Download the microk8s and core snaps:
```bash
$ snap download microk8s --channel=1.17/stable
$ snap download core
```

b. You should retrieve 4 files to the local directory, including: microk8s_1916.assert, microk8s_1916.snap, core_11993.assert & core_11993.snap.

c. Next, secure-copy the files to each offline node, by:
```bash
$ scp microk8s_1916.assert microk8s_1916.snap core_11993.assert core_11993.snap ubuntu@master_ip_private:~/

$ scp microk8s_1916.assert microk8s_1916.snap core_11993.assert core_11993.snap ubuntu@worker_node1_ip_private:~/

$ scp microk8s_1916.assert microk8s_1916.snap core_11993.assert core_11993.snap ubuntu@worker_node2_ip_private:~/
```

d. Once transferred to the offline nodes, Install core & Microk8s on each node:
```bash
$ sudo snap ack core_11993.assert
$ sudo snap install core_11993.snap

$ sudo snap ack microk8s_1916.assert
$ sudo snap install microk8s_1916.snap --classic
```

e. Now, microk8s should be installed on all the nodes. To configure permissions, add 'ubuntu' to 'Microk8s' group (do this for all offline nodes):
```bash
$ sudo usermod -a -G microk8s ubuntu
$ sudo chown -f -R ubuntu ~/.kube
$ newgrp microk8s
```

f. To check that microk8s is Installed correctly and running, on each node do:
```bash
$ microk8s.status #the output should say "microk8s is not running."

# To start Microk8s, use the stop/start method
$ microk8s.stop
$ microk8s.start
```

g. Checking status again will show it running now. It is also useful to enable the DNS, which will be useful during the deployment phase, so:
```bash
$ microk8s enable dns
```

#### ***2.3 Assigning Node Roles by Adding/Joining***

a. A VM can be assigned the 'Master' node role by using the 'add-node' command. Choose one of the 3 available offline Vms, then:
```bash
$ microk8s add-node #this will make this node the 'master' and generate joining commands for the 2 remaining 'workers'

>> Join node with: microk8s join <master_ip>:<port>/<token> # joining command output
```

b. Now, we are required to join the other VMs as worker nodes to the control plane, hence creating the k8 cluster. For this do:
```bash
$ microk8s join <master_ip>:<port>/<token>
```

c. Repeat steps a & b for the final remaining offline VM (resulting in a control plane & 2 worker nodes).

d. It is also important to have a namespace, which borders the pods that will be created later for deployment:
```bash
$ microk8s.kubectl create ns development
```

e. To check that the nodes are up & in a 'Ready' state do:
```bash
$ microk8s.kubectl get nodes -n development
```

**NOTE** At this stage, you should have:
- Microk8s Installed on ALL offline VMs.
- A Microk8s cluster with a control plane & worker nodes X2.
- Dns addon enabled.
- A 'Development' NameSpace in which deployment will occur later.
  

#### ***2.4 Install Docker on Bastion & Master Node***
a. Install Docker on Bastion:
```bash
# Install Docker on Bastion
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

$ echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

$ sudo apt-get update

$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```

b. Add 'Docker' to Ubuntu group:
```bash
$ sudo usermod -aG docker ubuntu 
$ exit
```

c. Then, on the Bastion, create a docker folder to contain the .deb installation files & send to master node:
```bash
# Create Docker Folder & .Deb Files
$ mkdir docker_debs
$ touch ./docker_debs/docker_1.deb ./docker_debs/docker_2.deb ./docker_debs/docker_3.deb ./docker_debs/docker_4.deb

# Install Docker on Bastion & Save Files in Docker_Deb Folder
$ curl https://download.docker.com/linux/ubuntu/dists/focal/pool/stable/amd64/containerd.io_1.4.9-1_amd64.deb > ./docker_debs/docker_1.deb

$ curl https://download.docker.com/linux/ubuntu/dists/focal/pool/stable/amd64/docker-ce-cli_20.10.9~3-0~ubuntu-focal_amd64.deb > ./docker_debs/docker_2.deb

$ curl https://download.docker.com/linux/ubuntu/dists/focal/pool/stable/amd64/docker-ce-rootless-extras_20.10.9~3-0~ubuntu-focal_amd64.deb > ./docker_debs/docker_3.deb

$ curl https://download.docker.com/linux/ubuntu/dists/focal/pool/stable/amd64/docker-ce_20.10.9~3-0~ubuntu-focal_amd64.deb > ./docker_debs/docker_4.deb

# Secure-Copy Across the Docker Folder to Master Node
$ scp -r docker_debs ubuntu@<master_node_ip>:~/
``` 

d. SSH into master plane & install docker using "docker_deb" folder:
```bash
# Install Docker in Offline Master Node
$ sudo dpkg -i docker_debs/docker_1.deb docker_debs/docker_2.deb docker_debs/docker_3.deb docker_debs/docker_4.deb

# Add Docker to 'Ubuntu' Group for Permissions
$ sudo usermod -aG docker ubuntu

# Exit to Affect User Group Changes
$ exit
```


#### ***2.5 Pull Docker Image on Bastion, Transfer to Master Node & Load***

a. Now, it is required to pull a docker image, which will later be implemented for deployment. Pull a basic nginx image & save it as a .docker file (then send to master node):
```bash
# Pull & Transfer Image to Offline Master Node
$ docker pull nginx
$ docker save -o nginx_image.docker nginx
$ scp nginx_image.docker ubuntu@master_node_ip:~/
```

b. Load nginx image on master node, then 
```bash
# Load nginx Image
$ docker load -i nginx_image.docker

# Save Image as .tar & Import
$ sudo docker save nginx > nginx.tar
$ sudo microk8s ctr image import nginx.tar
$ sudo microk8s ctr images ls
```

#### ***2.6 Attempt Deployment of nginx Application***
a. Create a deployment.yml file in the master node that includes within it the service too (for nginx):
```bash
apiVersion: apps/v1
kind: Deployment

metadata:
  name: nginx-app
  labels:
    app: webapp

spec:
  replicas: 2
  selector:
    matchLabels:
      app: webapp
  template:
    metadata:
      labels:
        app: webapp
    spec:
      containers:
      - name: nginx
        image: nginx:local
        ports:
        - containerPort: 80

---
apiVersion: v1
kind: Service
metadata:
  name: svc-cluster
spec:
  selector:
    app: webapp
  ports:
    - protocol: TCP
      port: 80
```

b. Now, we are required to apply the deployment.yml file, to deploy & create a service in the 'development' workspace:
```bash
# deploy (will create deployment + cluster-ip service) in 'development' namespace
$ microk8s.kubectl apply -f deployment.yml -n development
```

#### ***2.7 Issues with Deployment & Possible Solution***
Albeit the cluster being setup in an offline environment with a control plane (master node) and worker nodes (x2), there are issues with deployment. The nginx docker image can be used through docker in the master node & created (plus can make pods out of the image), however, when attempted through k8 deployment, the nginx image is unable to be pulled - hence, the pods do not become 'ready'. The error thrown is: 
```bash
$ microk8s.kubectl get pods -n development

# see under 'events':
>> Warning  FailedCreatePodSandBox  12h (x178 over 15h)  kubelet, ip-172-31-99-70  Failed to create pod sandbox: rpc error: code = Unknown desc = failed to get sandbox image "k8s.gcr.io/pause:3.1": failed to pull image "k8s.gcr.io/pause:3.1": failed to resolve image "k8s.gcr.io/pause:3.1": no available registry endpoint: failed to do request: Head "https://k8s.gcr.io/v2/pause/manifests/3.1": dial tcp 74.125.193.82:443: i/o timeout.
```

A possible solution may be to utilise an independant VM as a Docker registry and attempt to pull images via that method.

## Section 3 - Resources Utilised for Project

- [Offline Installation] https://microk8s.io/docs/install-alternatives
- [Cluster Creation] https://microk8s.io/docs/clustering
- [Docker Offline Installation] https://docs.docker.com/engine/install/ubuntu/
- [Docker Image Pull in Offline Seeting] https://stackoverflow.com/questions/48125169/how-run-docker-images-without-connect-to-internet

















  