# 1. Creating an Offline Raspberry Pi K8 Cluster

# 2. Contents
- [1. Creating an Offline Raspberry Pi K8 Cluster](#1-creating-an-offline-raspberry-pi-k8-cluster)
- [2. Contents](#2-contents)
  - [2.1. Introduction](#21-introduction)
- [3. Pre-requisites](#3-pre-requisites)
- [4. Installing `MicroK8s`](#4-installing-microk8s)
  - [4.1. Offline Snaps](#41-offline-snaps)
- [5. Starting the Cluster](#5-starting-the-cluster)
  - [5.1. Control Plane](#51-control-plane)
    - [5.1.1. Node](#511-node)
  - [5.2. Docker](#52-docker)
    - [5.2.1. Online Installation](#521-online-installation)
    - [5.3. Offline Installation](#53-offline-installation)
    - [5.3.1. Pulling a Docker Image for Offline Use](#531-pulling-a-docker-image-for-offline-use)
- [6. Kubernetes Set-up](#6-kubernetes-set-up)
  - [6.1. Namespace](#61-namespace)
  - [6.2. Deployment Files](#62-deployment-files)

## 2.1. Introduction

Here we go through the steps required to take your RPi cluster which was has been previously been set up and install `MicroK8s` a lightweight upstream Kubernetes.

The main cluster is offline but has a bastion connected through an ethernet port. We will be using SSH to transfer files to the offline machines but alternatively you could use a usb drive or another method.

# 3. Pre-requisites

Before we begin installing and setting up a RPi K8 cluster we will need the following:

| Pre-requisite | Description |
| :---: | :--- |
| RPi Cluster | We will be using a cluster of 3 RPis with 1 being the control plane and 2 being nodes. The cluster must already be built and have an OS, we are using Ubuntu 20.04 and many of the commands will be specific for this OS (or version). |
| Online Machine | We will need an online machine to download files which will be installed on the offline cluster. We are using one another RPi which has WiFi enabled. |
| Ability to Transfer Data | We will need a method to transfer files from the online machine to the offline cluster, we are using ethernet and SSH |

# 4. Installing `MicroK8s`

The end goal is to have the RPis acting as kubernetes cluster.

We are using `MicroK8s` upstream Kubernetes as it is lightweight and easy to install making it perfect for using on an offline RPi cluster.

## 4.1. Offline Snaps

`MicroK8s` is a most easily installed using it's `snap` which is a package manager specific to Ubuntu.

On your online machine you will need to download the snap, we are also downloading the `ubuntu-core` `snap` as it recommended for offline use.

```
snap download ubuntu-core
snap download microk8s --channel=1.17/stable
```
*Note* we are installing a slightly older stable version.

We then copy the `snap` files to the offline machine we want to install it on, we used `scp`.

This will include both the `.assert` and `.snap` files for both snaps.

We then need to `ack` the `.assert` files and then `install` the `snap` files on your offline machine.

```
sudo snap ack ./repo/ubuntu-core*.assert;
sudo snap install ./repo/ubuntu-core*.snap;
sudo snap ack ./repo/microk8s*.assert;
sudo snap install ./repo/microk8s*.snap --classic;
```

*Note* you will need to `install` the `microk8s` `snap` in `--classic` mode in order for it to be succesful.

We then need to reboot the RPi the snaps were installed on for these snaps to take effect:

```
sudo reboot
```

We then repeat this for each RPi in the cluster.

The scipt in `RPi_k8/microk8s_install.sh` should streamline this process.

# 5. Starting the Cluster

Now that the `microk8s` snap has been installed on all of the required RPis we need to set the control plane and the nodes.

## 5.1. Control Plane

On the RPi that you want to make the control plane input the command:

```
sudo microk8s.add-node
```

This will output a connection string in the form:

```
<master_ip>:<port>/<token>
```
You will need to copy or save this to use inside a node.

### 5.1.1. Node

Now inside a node you need to join the control plane with the folowing command:

```
microk8s.join <master_ip>:<port>/<token>
```

The node should now be connected to the control plane.

**To connect another node, a new connection string needs to be generated.**

So you need to repeat the control plane step.

After a short time the nodes should then be visible to the control plane by:

```
sudo microk8s.kubectl get node
```

The nodes will appear as `NotReady`.

This is a problem with memory being limited, to solve this we need to alter the config of a `cgroup`.

To do this we need to alter:

```
/boot/firmware/cmdline.txt 
```

We need to add the following options to the file somewhere in the same line as the other options:

```
cgroup_enable=cpuset cgroup_enable=memory cgroup_memory=1
```

## 5.2. Docker

To run containers within our cluster we will need a containerisation tool I will be using Docker.

This must be installed in the offline control plane as this is where the containers will need to be deployed.

The bastion machine will also need docker as it will need to pull container images from the internet which will be moved to the offline machine.

### 5.2.1. Online Installation

For installation of Docker on an online machine I followed the standard documentation (here)[https://docs.docker.com/engine/install/ubuntu/] for a Ubuntu machine.

This involved the following commands:

```bash
 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg


echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

### 5.3. Offline Installation

To install Docker on the control plane RPi we need to dowload the `.deb` files on the bastion RPi.

The available `.deb` files can be found here https://download.docker.com/, you will then need to navigate to the version to suit your OS and download the latest version of each package.

Below is a script which will download each required file and place them into a folder.

```
mkdir docker_debs
touch ./docker_debs/docker_1.deb ./docker_debs/docker_2.deb ./docker_debs/docker_3.deb ./docker_debs/docker_4.deb

curl https://download.docker.com/linux/ubuntu/dists/focal/pool/stable/arm64/containerd.io_1.4.12-1_arm64.deb > ./docker_debs/docker_1.deb
curl https://download.docker.com/linux/ubuntu/dists/focal/pool/stable/arm64/docker-ce-cli_20.10.11~3-0~ubuntu-focal_arm64.deb > ./docker_debs/docker_2.deb
curl https://download.docker.com/linux/ubuntu/dists/focal/pool/stable/arm64/docker-ce-rootless-extras_20.10.11~3-0~ubuntu-focal_arm64.deb > ./docker_debs/docker_3.deb
curl https://download.docker.com/linux/ubuntu/dists/focal/pool/stable/arm64/docker-ce_20.10.11~3-0~ubuntu-focal_arm64.deb > ./docker_debs/docker_4.deb
```

This folder can then be copied to the offline control plane where you can then install the packages using:

```bash
sudo dpkg -i docker_debs/docker_1.deb docker_debs/docker_2.deb docker_debs/docker_3.deb docker_debs/docker_4.deb
```

A script for this process can be found in `install_docker.sh`.

`MicroK8s` also recommends we create the following file:

```
sudo touch /etc/docker/daemon.json
```

We then need to add this code to the file:

```json
{
    "insecure-registries" : ["localhost:32000"] 
}
```

They also recommend altering the IPtables policy to enable traffic forwarding:

```bash
sudo iptables -P FORWARD ACCEPT
```

### 5.3.1. Pulling a Docker Image for Offline Use

To have an image from Docker Hub on the offline machine fisrt we must download the image on to the online machine.

```
sudo docker pull <image-name>
```

We can then save the image to a file which can then be copied (the file can be called anything).

```
sudo docker save -o <image_file_name>.docker <image-name>
```

Once you have transfered this file onto the offline machine we need to load the image:

```
sudo docker load -i <image_file_name>.docker
```

The image will now be available on the offline machine for use in the standard way.

We also need to make the Docker image available to MicroK8s by exporting the image from the local Docker daemon and “inject” it into the MicroK8s image cache like this:

```
docker save <image-name> > <my-image-name>.tar
microk8s ctr image import <my-image-name>.tar
```

# 6. Kubernetes Set-up

Here I will go through an example set-up to be used on the RPi cluster.

It will involve creating a `namespace` to work in and then creating deployment YAML files to set up an `nginx` container on the nodes and have a `Cluster IP` service to make it viewable on a browser.

## 6.1. Namespace

I am going to set up a `development` `namespace` with the following JSON file:

```
{
  "apiVersion": "v1",
  "kind": "Namespace",
  "metadata": {
    "name": "development",
    "labels": {
      "name": "development"
    }
  }
}
```

We can then create the `namespace` using:

```
sudo microk8s.kubectl create -f namespace-dev.json
```

## 6.2. Deployment Files

We now need a deployment file to set up our pods. We want a simple `nginx` container to be run so first we needed to download this image from docker as in Section 5.3.1.

```yaml
apiVersion: apps/v1
kind: Deployment

metadata:
  name: nginx-app
  labels:
    app: webapp

spec:
  replicas: 2
  selector:
    matchLabels:
      app: webapp
  template:
    metadata:
      labels:
        app: webapp
    spec:
      containers:
      - name: nginx
        image: nginx:local
        ports:
        - containerPort: 80
```

We can then apply this:

```
sudo microk8s.kubectl apply -f deployment.yml -n development
```

We then want to be able to access these pods from a singular IP. We will use a Cluster IP service for this.

```yml
apiVersion: v1
kind: Service
metadata:
  name: svc-cluster
spec:
  selector:
    app: webapp
  ports:
    - protocol: TCP
      port: 80
```

Again we want to apply this:

```
sudo microk8s.kubectl apply -f service.yml -n development
```