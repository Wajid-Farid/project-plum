# Documentation of Raspberry Pi Build

### Contents

- [Documentation of Raspberry Pi Build](#documentation-of-raspberry-pi-build)
    - [Contents](#contents)
  - [Introduction](#introduction)
  - [Inventory](#inventory)
  - [Build Process](#build-process)
  - [Software Setup](#software-setup)
    - [Flashing OS](#flashing-os)
    - [SSH into RPi](#ssh-into-rpi)
      - [Ethernet Connection](#ethernet-connection)
    - [Bastion Setup](#bastion-setup)
      - [Wifi Connection](#wifi-connection)
      - [Desktop](#desktop)
      - [`tmate`](#tmate)
  - [Air-Gapped System](#air-gapped-system)
    - [Using `apt-offline` to update offline machine](#using-apt-offline-to-update-offline-machine)
    - [Use `apt-offline` to install a new package](#use-apt-offline-to-install-a-new-package)

## Introduction

This documentation gives a step-by-step guide on how to build a RPi (Raspberry Pi) cluster. The intention is for this cluster to be used as an offline Kubernetes cluster with one RPi being the Control Plane and two of the RPis as nodes. 

This documentation does not include the setup of Kubernetes but does outline how to install an OS on the RPi and installing and updating packages in an offline environment.

Due to COVID some of this work needed to be completed remotely meaning we needed to create a way of accessing the offline RPis online. This was achieved using another RPi as a bastion with WiFi access, this documentation also outlines how this was implemented and how we could collaborate on the RPis remotely using `tmate`.

There are also some scripts that can be found in `RPi_scripts` that should streamline some of the processes.

## Inventory

To set up an RPi cluster you will need the following items:

| Quantity | Item | Description |
|:---:|:---|:---|
| 4 | Raspberry Pi 4 Model B 8GB RAM | These are the machines that will form the cluster. We are using 4 as we are using 1 as a bastion 1 as the control plane and 2 as nodes in the cluster. |
| 4 | microSD card | You need 1 per RPi, this acts as the storage for the RPi. |
| 1 | Ethernet Switch | This will be used to connect all of the RPis and to your laptop. |
| 5 | Ethernet Cable | This will be used with the ethernet switch to connect the RPis. |
| 1 | Multi-port Power Supply | This will provide power for the RPis. |
| 4 | Power Cables | We required USB A to USB C cables but depends on your power supply. |
  
**Optional**

| Quantity | Item | Description |
|:---:|:---|:---|
| 1 | RPi Case | We are using a rack type which holds 4 RPis, It should be possible to have them not in cases but helps to keep them safe and easier to transport. |
| 1 | USB A Keyboard | This will be used to plug into the RPi but isn't necessary if you can conncect with a laptop. |
| 1 | UBS A Mouse | This will be used to plug into the RPi but isn't necessary if you can conncect with a laptop. |
| 1 | Micro HDMI to HDMI Display Cable | Required if connecting to monitor for intitial setup or using a GUI. |

## Build Process

This is a step by step guide on how to build the RPi case and connect the RPis.

*Note:* a majority of this will only be relevant if using the same case.

| Step Number | Step | Description | Image |
| :---: | :---: | :--- | :---: |
| 1 | Heat Sinks | Install heatsinks on all of the RPis as shown. | ![alt text](RPi_build_images/heatsink_install.jpeg) |
| 2 | Bottom Standoffs | Screw the widerheaded screws into the bottom of the long standoffs through the bottom acrylic piece. *Note* the orientation and remember to remove the plastic film on the acrylic piece. | ![alt text](RPi_build_images/build_1.jpeg) ![alt text](RPi_build_images/build_3.jpeg) |
| 3 | Install Small Standoffs | Screw the smallest screws into the short standoffs through the acrylic piece as shown. *Note* orientation. | ![alt text](RPi_build_images/build_5.jpeg) ![alt text](RPi_build_images/build_6.jpeg) |
| 4 | Install Bottom RPi | Place the RPi onto the short standoffs and use secure with the nuts. *Note* orientation. |  |
| 5 | Install Fan | Using the long fan screws and nuts secure the fan to one of the middle acrylic pieces. *Note* orientation of all parts. | ![alt text](RPi_build_images/build_9.jpeg) ![alt text](RPi_build_images/build_10.jpeg) |
| 6 | Install Short Standoffs | Install the short standoffs to the middle acrylic piece similar to step 3. |  |
| 7 | Install Middle RPi | Place the RPi on the short stand off and secure with nuts similar to step 4. *Note* orientation. |  |
| 8 | Repeat 5-7 | Repeat steps 5 to 7 and install the remaining RPis onto the remaining middle acrylic pieces. |  |
| 9 | Connect Fan | Connect the fan cables from the fan on the above board to the RPi below as shown with red to GPIO pin 4 (5V) and black to GPIO pin 6 (ground). | ![alt text](RPi_build_images/build_13.jpeg) |
| 10 | Place Middle RPi | Place RPi with middle acrylic piece ontop of bottom RPi's long standoffs | ![alt text](RPi_build_images/build_14.jpeg) |
| 11 | Install Long Standoffs | Screw the long standoffs onto the other long stand offs with one of the middle RPis between them. | ![alt text](RPi_build_images/build_15.jpeg) ![alt text](RPi_build_images/build_16.jpeg) |
| 12 | Repeat 9-11 | Repeat steps 9 to 11 for the remaining RPis. | ![alt text](RPi_build_images/build_17.jpeg) ![alt text](RPi_build_images/build_18.jpeg) |
| 13 | Install Top Fan | Screw the long fan screws through the top acrylic piece, fan cover, and the fan as showm. *Note* orientation and remember to remove the plastic film from the acrylic piece. | ![alt text](RPi_build_images/build_19.jpeg) ![alt text](RPi_build_images/build_20.jpeg) |
| 14 | Install Top Acrylic | Connect the fan as before and place the top acrylic piece on to the long standoffs and secure with the screw caps. | ![alt text](RPi_build_images/build_23.jpeg) |

## Software Setup

This section is a guide on setting up the software on the RPi.

### Flashing OS

Now that the physical system has been created we need to install an OS on the RPis.

I am installing Ubuntu 20.04 and have used [this](https://ubuntu.com/tutorials/how-to-install-ubuntu-on-your-raspberry-pi#1-overview) comprehensive guide.

A quick overview:

1. Install Raspberry Pi Imager on own computer
2. Plug SD card into computer
3. Open RPi Imager
4. Select OS
5. Select SD card
6. Write to SD card
7. Put SD card in RPi
8. Power on RPi
9. Log into RPi (will need keyboard and monitor plugged into )

      - user: ubuntu
      - password: ubuntu
      - Then prompted to give new password

OS should now be installed on the RPi.

This will need repeating for all RPis.

### SSH into RPi

It will be useful for offline installation of software to be able to SSH into the RPis.

#### Ethernet Connection

You will need to connect an ethernet cable to your Mac (using a dongle) which will then need connecting to your ethernet switch. You then need to connect the switch to the RPi with another ethernet cable and ensure everything is turned on.

You now need to ensure that your Mac allows you to share internet, I used [this](https://raspberrypi-guide.github.io/networking/create-direct-ethernet-connection) guide to do this and for the remainder of this section.

You will need to go into your `System Preferences` > `Sharing` and ensure `Internet Sharing` and `USB 10/100/1000 LAN` are both selected.

You then need to open a terminal window and type:

```
ifconfig
```

You should then see an interface called `bridge100` within which there will be a section called `inet` in which there will be an IP address which is for your machine.

The IP of the RPi will be this address plus 1 in the last section.

Alternatively if you can access the RPi directly you can:

```
ip addr show
```

This will show the RPi address.

Now that you have the IP address of the RPi you can SSH:

```
ssh ubuntu@<IP.of.your.RPi>
```

You will then be prompted to enter your password.

You are now inside the RPi.

If all of the RPis are connected to the switch and turned on you should be able to SSH from one RPi to the others.

For reference the list of IPs for my cluster:

| IP          | RPi           |
| :---------: | :-----------: |
| 192.168.2.2 | Bastion       |
| 192.168.2.3 | Control Plane |
| 192.168.2.4 | Node 1        |
| 192.168.2.5 | Node 2        |

You may have to experiment to find which one is which but when using an ethernet switch, the number in the final octet should be designated by the numbers on the switch.

### Bastion Setup

I intend to use one of the RPis as a bastion to allow remote access to the cluster for myself and colleagues.

To do this first the chosen RPi will need access to the router through ethernet or wifi.

#### Wifi Connection

In my case I am using wifi.

To allow the bastion RPi to connect to the router we need to alter some config files.

I used [this](https://huobur.medium.com/how-to-setup-wifi-on-raspberry-pi-4-with-ubuntu-20-04-lts-64-bit-arm-server-ceb02303e49b) documentation to do this.

You will need to edit `/etc/netplan/50-cloud-init.yaml` and add your wifi router this will look similar to below:

```yml
network:
    ethernets:
        eth0:
            dhcp4: true
            optional: true
    version: 2
    wifis:
        wlan0:
            optional: true
            access-points:
                "<your wifi>":
                    password: "<your-wifi-password>"
            dhcp4: true
```

*Note* this is a YAML file so the indentation is spaces not tabs.

You will then need to reboot the RPi:

```
sudo reboot
```

#### Desktop

I am also installing a desktop for the bastion, this will allow you to open multiple terminal windows so that we can have multiple people working on the RPis using `tmate` (see next section).

I am using xubuntu as it's light weight.

```
sudo apt install xubuntu-desktop
```

You will then need to reboot:

```
sudo reboot
```

#### `tmate`

I am using `tmate` which allows others others to access a terminal within a machine remotely.

To do this first you must intsall `tmate`:

```
sudo apt install tmate
```

You will then be able to start a session simply by putting the command `tmate` into the terminal on the bastion.

You will then see a list of ways of accessing and you can send one of those ways to your colleague who can then run the command in their terminal and will now have ssh access to the terminal on the bastion.

## Air-Gapped System

We are going to use `apt-offline` to install updates and packages on the RPis that aren't connected to the internet as we can't use standard `apt-get` commands to pull them from the internet.

First we need to install `apt-offline`.

On the online machine:

```
sudo apt install apt-offline
```

You then need to download the `.deb` file on the online machine:

`curl http://ports.ubuntu.com/pool/universe/a/apt-offline/apt-offline_1.8.2-1_all.deb --output <file/path/to/store/file>`

For convienience I am also going to install a package to automate password entry.

*Note:* this is not recommended, you should use ssh keys and transfer them to all of the machines.

```
sudo apt-get install sshpass
```

You can then `scp` the `.deb` file over and run it in the offline machine to install `apt-offline`:

```
sudo dpkg -i apt-offline
```

### Using `apt-offline` to update offline machine

The process of using `apt-offline` goes: (OFFM = offline machine, ONM = online machine)

1. OFFM - create signature file

```
sudo apt-offline set --update --upgrade apt-offline.si
```

2. Copy signature file OFFM > ONM

On the ONM:

```
scp <user>@<ip.add.off.machine>:apt-offline.sig </path/to/target>
```

3. ONM - Find updates

```
sudo apt-offline get --bundle bundle.zip <path/to/>apt-offline.sig
```

This finds all the updates required based on the signature file.

4. Copy zip file ONM > OFFM

On the ONM:

```
scp bundle.zip <user>@<ip.add.off.machine>:/path/to/target
```

5. OFFM - Install the bundle files

```
sudo apt-offline install bundle.zip
```

```
sudo apt-get upgrade
```

The OFFM should now be upgraded.

### Use `apt-offline` to install a new package

1. OFFM - create signature file

```
sudo apt-offline set --install-packages <package-name> --update apt-offline.sig
```

1. Copy signature file OFFM > ONM

On the ONM:

```
scp <user>@<ip.add.off.machine>:apt-offline.sig </path/to/target>
```

3. ONM - Find updates

```
sudo apt-offline get --bundle bundle.zip apt-offline.sig
```

This finds all the files required based on the signature file including dependencies.

1. Copy zip file ONM > OFFM

On the ONM:

```
scp bundle.zip <user>@<ip.add.off.machine>:/path/to/target
```

5. OFFM - Install the bundle files

```
sudo apt-offline install bundle.zip
```

```
sudo apt-get install <package-name>
```

The OFFM should now have the package and all dependencies.

There scripts to install `apt-offline` - `install_apt-offline.sh`, find and install updates on an offline machine - `apt-offline_update.sh`, and one to install packages on the offline machine - `apt-offline_package.sh`.

All should be run on the online machine.