
# Assign first input to the IP of the RPi that you want to install on
RPI_IP=$1
USER=$2
PASSWORD=$3

# Download .deb file

curl http://ports.ubuntu.com/pool/universe/a/apt-offline/apt-offline_1.8.2-1_all.deb --output ./apt-offline

# Transfer folder with deb package

sshpass -p $PASSWORD scp ./apt-offline $USER@$RPI_IP:./

# Remove .deb

rm apt-offline

# Install packages inside offline machine

sshpass -p $PASSWORD ssh $USER@$RPI_IP '
sudo dpkg --configure -a;
sudo apt-get install apt-offline -y
'