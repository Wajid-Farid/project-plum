# Assign first input to the IP of the RPi that you want to install on
RPI_IP=$1
USER=$2
PASSWORD=$3
PACKAGE=$4

# Create signature file

sshpass -p $PASSWORD ssh $USER@$RPI_IP '
sudo apt-offline set --install-packages $PACKAGE --update apt-offline.sig
'

# Copy signature file OFFM > ONM

sshpass -p $PASSWORD scp $USER@$RPI_IP:apt-offline.sig .

# ONM - Find package and download

sudo apt-offline get --bundle bundle.zip apt-offline.sig

# Copy zip file ONM > OFFM

sshpass -p $PASSWORD scp bundle.zip $USER@$RPI_IP:.

# OFFM - Install the bundle files
sshpass -p $PASSWORD ssh $USER@$RPI_IP '
sudo apt-offline install bundle.zip
'
sshpass -p $PASSWORD ssh $USER@$RPI_IP '
sudo apt-get install $PACKAGE -y;
rm apt-offline
'