# Assign first input to the IP of the RPi that you want to install on
RPI_IP=$1
USER=$2
PASSWORD=$3

./install_apt-offline.sh $RPI_IP $USER $PASSWORD
./apt-offline_update.sh $RPI_IP $USER $PASSWORD
./microk8s_install.sh $RPI_IP $USER $PASSWORD