# Assign first input to the IP of the RPi that you want to install on

RPI_IP=$1
USER=$2
PASSWORD=$3

# Create directory and files to store the docker .deb files 

mkdir docker_debs
touch ./docker_debs/docker_1.deb ./docker_debs/docker_2.deb ./docker_debs/docker_3.deb ./docker_debs/docker_4.deb

# Download all of the .deb files for the docker packages

curl https://download.docker.com/linux/ubuntu/dists/focal/pool/stable/arm64/containerd.io_1.4.12-1_arm64.deb > ./docker_debs/docker_1.deb
curl https://download.docker.com/linux/ubuntu/dists/focal/pool/stable/arm64/docker-ce-cli_20.10.11~3-0~ubuntu-focal_arm64.deb > ./docker_debs/docker_2.deb
curl https://download.docker.com/linux/ubuntu/dists/focal/pool/stable/arm64/docker-ce-rootless-extras_20.10.11~3-0~ubuntu-focal_arm64.deb > ./docker_debs/docker_3.deb
curl https://download.docker.com/linux/ubuntu/dists/focal/pool/stable/arm64/docker-ce_20.10.11~3-0~ubuntu-focal_arm64.deb > ./docker_debs/docker_4.deb

# Secure copy the directory to the offline machine

sshpass -p $PASSWORD scp ./docker_debs $USER@$RPI_IP:.

# Install docker packages

sshpass -p $PASSWORD ssh $USER@$RPI_IP '
sudo dpkg -i docker_debs/docker_1.deb docker_debs/docker_2.deb docker_debs/docker_3.deb docker_debs/docker_4.deb
'
