# Assign first input to the IP of the RPi that you want to install on
RPI_IP=$1
USER=$2
PASSWORD=$3

# Make a repo to store snaps in

mkdir repo

# Download required snaps

snap download ubuntu-core --target-directory=./repo/
snap download microk8s --channel=1.17/stable --target-directory=./repo/

# Copy repo folder to OFFM

sshpass -p $PASSWORD scp -r ./repo $USER@$RPI_IP:.

# Assert and install all snaps

sshpass -p $PASSWORD ssh $USER@$RPI_IP '
sudo snap ack ./repo/ubuntu-core*.assert;
sudo snap install ./repo/ubuntu-core*.snap;
sudo snap ack ./repo/microk8s*.assert;
sudo snap install ./repo/microk8s*.snap --classic;
rm -rf repo;
sudo reboot
'

# Remove repo to cleanup

rm -rf repo