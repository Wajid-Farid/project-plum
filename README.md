# Secure Container Platform Audit

This project is broken into three parts:

1. Kubespray installation audit
2. Micro K8s offering
3. Raspberry Pi cluster

You will find all relevant documentation and deliverables in the corresponding folders.